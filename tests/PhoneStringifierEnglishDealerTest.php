<?php

use PHPUnit\Framework\TestCase;
use App\DealerEnglish;
use App\PhoneStringifier;

class PhoneStringifierEnglishDealerTest extends TestCase
{
    function testSimpleCase()
    {
        //GIVEN
        $number = '5';
        $words = ['a', 'j', 'k', 'l', 'jkl'];
        $dealer = new DealerEnglish();
        //WHEN
        $phoneStringifier = new PhoneStringifier($dealer);
        $actual = $phoneStringifier->getMatchingStrings($number, $words);
        //THEN
        $expected = ['j', 'k', 'l'];
        $this->assertEquals($expected, $actual);
    }

    function testMultipleWords()
    {
        //GIVEN
        $number = '366';
        $words = ['foo', 'bar', 'fon', 'fo', 'on'];
        $dealer = new DealerEnglish();
        //WHEN
        $phoneStringifier = new PhoneStringifier($dealer);
        $actual = $phoneStringifier->getMatchingStrings($number, $words);
        //THEN
        $expected = ['foo', 'fon', 'fo', 'on'];
        $this->assertEquals($expected, $actual);
    }
}