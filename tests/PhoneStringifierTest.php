<?php

use App\Dealer;
use PHPUnit\Framework\TestCase;
use App\PhoneStringifier;


class PhoneStringifierTest extends TestCase
{

    function testEmptyWordArray()
    {
        //GIVEN
        $number = '5';
        $words = [];
        $dealer = $this->getMockBuilder(Dealer::class)
            ->disableOriginalConstructor()
            ->getMock();
        //WHEN
        $phoneStringifier = new PhoneStringifier($dealer);
        $actual = $phoneStringifier->getMatchingStrings($number, $words);
        //THEN
        $expected = [];
        $this->assertEquals($expected, $actual);
    }

    function testSimpleCase()
    {
        //GIVEN
        $number = '5';
        $words = ['a', 'j', 'k', 'l', 'jkl'];
        $dealer = $this->getMockBuilder(Dealer::class)
            ->disableOriginalConstructor()
            ->getMock();
        $dealer->method('getDealerNumberByString')
            ->willReturnOnConsecutiveCalls('2', '5', '5', '5', '555');
        //WHEN
        $phoneStringifier = new PhoneStringifier($dealer);
        $actual = $phoneStringifier->getMatchingStrings($number, $words);
        //THEN
        $expected = ['j', 'k', 'l'];
        $this->assertEquals($expected, $actual);
    }

    function testMultipleWords()
    {
        //GIVEN
        $number = '366';
        $words = ['foo', 'bar', 'fon', 'fo', 'on'];
        $dealer = $this->getMockBuilder(Dealer::class)
            ->disableOriginalConstructor()
            ->getMock();
        $dealer->method('getDealerNumberByString')
            ->willReturnOnConsecutiveCalls('366', '227', '366', '36', '66');
        //WHEN
        $phoneStringifier = new PhoneStringifier($dealer);
        $actual = $phoneStringifier->getMatchingStrings($number, $words);
        //THEN
        $expected = ['foo', 'fon', 'fo', 'on'];
        $this->assertEquals($expected, $actual);
    }
}