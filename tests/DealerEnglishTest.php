<?php

use PHPUnit\Framework\TestCase;
use App\DealerEnglish;

class DealerEnglishTest extends TestCase
{
    public function testGetDealerNumberByStringWhenStringIsNotValid()
    {
        $char = 'año';
        //THEN
        $this->expectException(InvalidArgumentException::class);
        //WHEN
        $dealer = new DealerEnglish();
        $dealer->getDealerNumberByString($char);
    }

    public function testGetDealerNumberByStringWhenStringIsEmpty()
    {
        $char = '';
        //WHEN
        $dealer = new DealerEnglish();
        $actual = $dealer->getDealerNumberByString($char);
        //THEN
        $expected = '';
        $this->assertEquals($expected, $actual);
    }

    public function testvalidateDealerNumberWhenDialIsNotANumber()
    {
        //GIVEN
        $deal = 'NotANumber';
        //THEN
        $this->expectException(InvalidArgumentException::class);
        //WHEN
        $dealer = new DealerEnglish();
        $dealer->validateDealerNumber($deal);
    }

    public function testvalidateDealerNumberWhenDialHasNotAValidNumber()
    {
        //GIVEN
        $deal = '3450';
        //THEN
        $this->expectException(InvalidArgumentException::class);
        //WHEN
        $dealer = new DealerEnglish();
        $dealer->validateDealerNumber($deal);
    }

    public function testGetDealerNumberByStringWhenStringIsValid()
    {
        $char = ' afhknqvy';
        //WHEN
        $dealer = new DealerEnglish();
        $actual = $dealer->getDealerNumberByString($char);
        //THEN
        $expected = "123456789";
        $this->assertEquals($expected, $actual);
    }

}