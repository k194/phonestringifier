### Versions

- Version 1.0 Unit tests 2h
- Version 2.0 Integration tests and refactor 2,5h
- Version 3.0 Docker php 3h

### Start docker container
```docker-compose up -d```

### Install dependencies
```docker-compose run composer install```

### Run tests
```docker-compose run php  /var/www/html/vendor/phpunit/phpunit/phpunit --configuration /var/www/html/phpunit.xml.dist /var/www/html/tests --teamcity```

