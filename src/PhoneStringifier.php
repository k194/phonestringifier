<?php

namespace App;

class PhoneStringifier
{
    private Dealer $dealer;

    public function __construct(Dealer $dealer)
    {
        $this->dealer = $dealer;
    }

    public function getMatchingStrings(string $number, array $strings): array
    {
        $this->dealer->validateDealerNumber($number);
        $matchingStrings = [];
        foreach ($strings as $string) {
            $dialerNumberString = $this->dealer->getDealerNumberByString($string);
            if (strpos($number, $dialerNumberString) !== FALSE) {
                $matchingStrings[] = $string;
            }
        }
        return $matchingStrings;
    }

}