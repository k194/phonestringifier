<?php


namespace App;


interface Dealer
{
    public function getDealerNumberByString(string $str): string;
    public function validateDealerNumber(string $dealerNumber): void;
}